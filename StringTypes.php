<?php

/**
 * Working on GIT by NetBeans IDE
 */

// There have four String Types.
$a = 320;

# 1. Single Quote. Non-Evaluate but confliclt with single quote into it.

$single = 'This is single qutoe /n goes here $a not working';

echo $single;

# 2. Double Quote. Evaluate but confliclt with double quote into it.

$double =  "This is double qutoe /n goes here $a working";

echo $double;

# 3. HereDoc. Evaluate and Not confliclt with double quote into it.

$HereDoc = <<<HereDoc
           $a 
HereDoc;

// 4. NowDoc. Non-Evaluate and Not confliclt with double quote into it.

$NowDoc = <<<'NowDoc'
    $a
NowDoc;

echo $NowDoc;

// End of file






